@extends("layouts.main-layout")

@section("content")

<!-- Page Banner Start -->
<div class="page-banner section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-title">
                    <h1 class="pb-30">About Us</h1>
                    <ul>
                        <li><a href="#">Hone</a>
                        </li>
                        <li><a href="#">About</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Banner End -->
<!-- About Area Start -->
<div class="about-area section-padding">
    <div class="bounce-top shape2">
        <img src="assets/img/shape/ab-shape-3.png" alt="">
    </div>
    <div class="item-roll shape1">
        <img src="assets/img/shape/ab-shape-2.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 md-mb-35">
                <div class="about-left">
                    <div class="about-img">
                        <img src="assets/img/about.png" alt="">
                    </div>
                    <div class="item-bounce about-shape">
                        <img src="assets/img/shape/ab-shape-1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="s-table">
                    <div class="s-tablec">
                        <div class="section-title section-title-right"> <span class="section-top">//  About Us</span>
                            <h2>We are the top law firm in Europe</h2>
                            <p class="pt-30 pb-35">

                                Wence & Partners is a leading corporate and commercial law firm in Europe with broad expertise and experience in
                                providing first-rate legal services for international and local corporations in all sectors of the economy.

                            </p>
                            <div class="about-buttom d-sm-flex">
                                <div class="about-btn"> <a class="attoyer-btn all-btn mr-100" href="#">Discover More</a>
                                </div>
                                <div class="about-signa">
                                    <img src="assets/img/signature.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Area End -->
<!-- Why Choose Ua Area Area Start -->
<div class="why-choose-area section-padding">
    <div class="item-roll choose1">
        <img src="assets/img/shape/ab-shape-02.png" alt="">
    </div>
    <div class="item-bounce choose2">
        <img src="assets/img/shape/ch-shape-2.png" alt="">
    </div>
    <div class="why-choose-img"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-8">
                <div class="section-title-left"> <span class="section-top">// Why Choose Us</span>
                    <h2 class="mb-55">Why We different from others</h2>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 mb-40">
                            <div class="choose-item d-inline-flex">
                                <div class="choose-icon"> <i class="flaticon-checked"></i>
                                </div>
                                <div class="choose-content">
                                    <h3>Intellectual Property</h3>
                                    <p>We practice intellectual property litigation, including patent, trademark etc.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="choose-item d-inline-flex">
                                <div class="choose-icon"> <i class="flaticon-checked"></i>
                                </div>
                                <div class="choose-content">
                                    <h3>Litigation</h3>
                                    <p> We handle a wide variety of cases, including complex commercial transactions,
                                        consumer litigation, class actions etc.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 sm-mt-30 sm-mb-30">
                            <div class="choose-item d-inline-flex">
                                <div class="choose-icon"> <i class="flaticon-checked"></i>
                                </div>
                                <div class="choose-content">
                                    <h3>Business Law</h3>
                                    <p>
                                        We provide services as outside general counsel, advising leading local and national corporations
                                        and other businesses on a full range of corporate law matters.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="choose-item d-inline-flex">
                                <div class="choose-icon"> <i class="flaticon-checked"></i>
                                </div>
                                <div class="choose-content">
                                    <h3>Employment and Labour Law </h3>
                                    <p>We have a team of dedicated Employment & Labor Law practitioners to handle our clients’
                                        most complex employment cases.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Why Choose Ua Area Area End -->
<!-- Counter Area Start -->
<div class="counter-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 md-mb-30">
                <div class="counter-item">
                    <ul class="d-inline-flex align-items-center">
                        <li><i class="flaticon-court"></i>
                        </li>
                        <li><span class="counter">72</span>
                            <p>Case Done</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6">
                <div class="counter-item">
                    <ul class="d-inline-flex align-items-center">
                        <li><i class="flaticon-presentation"></i>
                        </li>
                        <li><span class="counter">59</span>
                            <p>Happy Client’s</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 sm-mt-30 sm-mb-30">
                <div class="counter-item">
                    <ul class="d-inline-flex align-items-center">
                        <li><i class="flaticon-law"></i>
                        </li>
                        <li><span class="counter">42</span>
                            <p>Case Dismissed</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6">
                <div class="counter-item">
                    <ul class="d-inline-flex align-items-center">
                        <li><i class="flaticon-balance"></i>
                        </li>
                        <li><span class="counter">92</span>
                            <p>Charges Dropped</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Counter Area End -->
<!-- Testimonial Area Start -->
<div class="testimonial-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-6 sm-mb-30">
                <div class="section-title-left"> <span class="section-top">// Testimonial</span>
                    <h2>What they are Talking About Lawyer</h2>
                </div>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-6">
                <div class="testimonial-group">
                    <div class="testimonial-tiem">
                        <div class="testimonial-icon"> <i class="flaticon-straight-quotes"></i>
                        </div>
                        <p class="pb-30">Here are many variations of passages consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                        <div class="client-name d-inline-flex">
                            <div class="client-img mr-20">
                                <img src="assets/img/testminial-1.png" alt="">
                            </div>
                            <div class="client-content">
                                <h3>Al Mahmud</h3>
                                <p>Client</p>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-tiem">
                        <div class="testimonial-icon"> <i class="flaticon-straight-quotes"></i>
                        </div>
                        <p class="pb-30">Here are many variations of passages consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                        <div class="client-name d-inline-flex">
                            <div class="client-img mr-20">
                                <img src="assets/img/testminial-2.png" alt="">
                            </div>
                            <div class="client-content">
                                <h3>Jhon Deo</h3>
                                <p>Client</p>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-tiem">
                        <div class="testimonial-icon"> <i class="flaticon-straight-quotes"></i>
                        </div>
                        <p class="pb-30">Here are many variations of passages consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>
                        <div class="client-name d-inline-flex">
                            <div class="client-img mr-20">
                                <img src="assets/img/testminial-3.png" alt="">
                            </div>
                            <div class="client-content">
                                <h3>Al Mahmud</h3>
                                <p>Client</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Testimonial Area End -->
@endsection
