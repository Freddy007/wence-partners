@extends("layouts.main-layout")

@section("content")

<!-- Page Banner Start -->
<div class="page-banner section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-title">
                    <h1 class="pb-30">Contact Us</h1>
                    <ul>
                        <li><a href="#">Home</a>
                        </li>
                        <li><a href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Banner End -->
<!-- Contact Details Start -->
<div class="contact-list section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 md-mb-30">
                <div class="contact-item">	<i class="flaticon-placeholder"></i>
                    <h3 class="pt-20 pb-20">Location</h3>
                    <p>	9 Pickwick Rd, Corsham Wiltshire</p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 sm-mb-30">
                <div class="contact-item">	<i class="flaticon-envelope"></i>
                    <h3 class="pt-20 pb-20">Email</h3>
                    <p>info@wencepartners.com</p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="contact-item">	<i class="flaticon-call"></i>
                    <h3 class="pt-20 pb-20">Phone Number</h3>
                    <p>+(151 - 589 - 548</p>
                    <p>+(141 - 599 - 048</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact Details End -->
<!-- Contact Area Start -->
<div class="contact-area section-padding">
    <div class="contact-right"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="section-title-left"> <span class="section-top">//  Free Quote</span>
                    <h2 class="fff mb-60">Get a free Consultation</h2>
                    <div class="from-area">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" id="fullName" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" class="form-control" id="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="subject" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="message" rows="2" placeholder="Message"></textarea>
                            </div>
                            <button type="submit" class="attoyer-btn">Send Massage</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact Area End -->
<!-- Map Area Start -->
<div class="contact-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d638290.5559587004!2d-2.4860800255572433!3d51.323588082477585!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487137c28922c829%3A0x648834f891bd9734!2sWiltshire%2C%20UK!5e0!3m2!1sen!2sgh!4v1641638448435!5m2!1sen!2sgh" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
{{--    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.8404599049163!2d144.95373931582537!3d-37.81720574201448!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sbd!4v1639647690771!5m2!1sen!2sbd" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>--}}
</div>
<!-- Map Area End -->

@endsection
