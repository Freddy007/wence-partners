<div class="col-lg-4">
    <div class="widget-area">
        <div class="services widget-item">
            <h3>Services List</h3>
            <ul>
                <li>
                    <a href="#">
                        Cash Investment
                        <i class='bx bx-right-arrow-alt'></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Personal Insurance
                        <i class='bx bx-right-arrow-alt'></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Education Loan
                        <i class='bx bx-right-arrow-alt'></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        Financial Planning
                        <i class='bx bx-right-arrow-alt'></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="download widget-item">
            <h3>Download</h3>
            <ul>
                <li>
                    <a href="#">
                        <i class='bx bxs-file-pdf'></i>
                        Presentation pdf
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class='bx bx-notepad'></i>
                        Wordfile.doc
                    </a>
                </li>
            </ul>
        </div>
        <div class="contact widget-item">
            <h3>Contact</h3>
            <form>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Your email">
                </div>
                <div class="form-group">
                    <textarea id="your-message" rows="8" class="form-control" placeholder="Message"></textarea>
                </div>
                <button type="submit" class="btn common-btn">
                    Send Message
                    <span></span>
                </button>
            </form>
        </div>
        <div class="consultation">
            <img src="assets/img/services/service-details4.jpg" alt="Details">
            <div class="inner">
                <h3>Need Any Consultation</h3>
                <a class="common-btn" href="contact.html">
                    Send Message
                    <span></span>
                </a>
            </div>
        </div>
    </div>
</div>
