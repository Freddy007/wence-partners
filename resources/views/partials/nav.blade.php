
<div class="main-nav two">
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-light">
            <a class="navbar-brand" href="index.html">
{{--                <img src="assets/img/logo-three.png" alt="Logo">--}}
{{--                <img src="assets/img/logo-three.png" alt="Logo">--}}
                <img src="assets/img/logo_2.png" alt="Logo">

            </a>
            <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

                @include("partials.nav_items")

                <div class="side-nav">
                    <div class="dropdown nav-flag-dropdown">
                        <button class="btn dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <img src="assets/img/flag1.jpg" alt="Flag">
                            Eng
                            <i class='bx bx-chevron-down'></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">
                                <img src="assets/img/flag2.jpg" alt="Flag">
                                Ger
                            </a>
                            <a class="dropdown-item" href="#">
                                <img src="assets/img/flag3.jpg" alt="Flag">
                                Isr
                            </a>
                            <a class="dropdown-item" href="#">
                                <img src="assets/img/flag4.jpg" alt="Flag">
                                USA
                            </a>
                        </div>


                    </div>
                    <a class="consultant-btn" href="http://monezbank.herokuapp.com/login" target="_blank">
                        Internet Banking
                    </a>
                </div>
            </div>
        </nav>
    </div>
</div>
