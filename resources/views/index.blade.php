@extends("layouts.main-layout")

@section("content")


{{--<!-- Top Bar Start -->--}}
{{--<div class="top-bar">--}}
{{--    <div class="container">--}}
{{--        <div class="row align-items-center">--}}
{{--            <div class="col-xl-6 col-lg-6 col-md-8">--}}
{{--                <div class="top-left d-i-block">--}}
{{--                    <ul>--}}
{{--                        <li><a href="#"><i class="flaticon-call"></i></a> +(141 - 589 - 548</li>--}}
{{--                        <li><a href="#"><i class="flaticon-envelope"></i></a> infoattoyer@gmail.com</li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-xl-6 col-lg-6 col-md-4">--}}
{{--                <div class="top-right d-i-block text-right">--}}
{{--                    <ul>--}}
{{--                        <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>--}}
{{--                        </li>--}}
{{--                        <li><a href="#"><i class="flaticon-twitter"></i></a>--}}
{{--                        </li>--}}
{{--                        <li><a href="#"><i class="flaticon-linkedin"></i></a>--}}
{{--                        </li>--}}
{{--                        <li><a href="#"><i class="flaticon-vimeo"></i></a>--}}
{{--                        </li>--}}
{{--                        <li><a href="#"><i class="flaticon-instagram"></i></a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- Top Bar End -->--}}
{{--<!-- Header Menu Start -->--}}
{{--<div class="header-menu">--}}
{{--    <div class="container">--}}
{{--        <div class="row align-items-center">--}}
{{--            <div class="col-xl-2 col-lg-2 col-md-7">--}}
{{--                <div class="menu-logo">--}}
{{--                    <a href="index.html">--}}
{{--                        <img src="assets/img/logo.png" alt="">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="responsive-menu"></div>--}}
{{--            </div>--}}
{{--            <div class="col-xl-9 col-lg-9 col-md-2">--}}
{{--                <div class="main-menu">--}}
{{--                    <ul id="mobilemenu">--}}
{{--                        <li class="menu-icon"><a href="#">Home</a>--}}
{{--                            <ul class="submenu">--}}
{{--                                <li><a href="index.html"><i class="flaticon-double-right-arrows-angles"></i> Home style 1</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="menu-icon"><a href="#">Services</a>--}}
{{--                            <ul class="submenu">--}}
{{--                                <li><a href="services-1.html"><i class="flaticon-double-right-arrows-angles"></i> Services-1</a>--}}
{{--                                </li>--}}
{{--                                <li><a href="services-2.html"><i class="flaticon-double-right-arrows-angles"></i> Services-2</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="menu-icon"><a href="#">Page</a>--}}
{{--                            <ul class="submenu">--}}
{{--                                <li><a href="about.html"><i class="flaticon-double-right-arrows-angles"></i> About Us</a>--}}
{{--                                </li>--}}
{{--                                <li><a href="pricing.html"><i class="flaticon-double-right-arrows-angles"></i> Pricing</a>--}}
{{--                                </li>--}}
{{--                                <li><a href="faq.html"><i class="flaticon-double-right-arrows-angles"></i> FAQ</a>--}}
{{--                                </li>--}}
{{--                                <li><a href="team.html"><i class="flaticon-double-right-arrows-angles"></i> Team</a>--}}
{{--                                </li>--}}
{{--                                <li><a href="testimonial.html"><i class="flaticon-double-right-arrows-angles"></i> Testimonial</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="menu-icon"><a href="#">News</a>--}}
{{--                            <ul class="submenu">--}}
{{--                                <li><a href="blog-grid.html"><i class="flaticon-double-right-arrows-angles"></i> Blog Grid</a>--}}
{{--                                </li>--}}
{{--                                <li><a href="blog-details.html"><i class="flaticon-double-right-arrows-angles"></i> Blog Details</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li class="menu-icon"><a href="contact.html">Contact</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-xl-1 col-lg-1 col-md-3">--}}
{{--                <div class="menu-icon">--}}
{{--                    <ul class="d-flex">--}}
{{--                        <li><span class="search-icon open"><i class="flaticon-search"></i></span>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <div class="hamburger-icon"> <i class="flaticon-menu"></i>--}}
{{--                            </div>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="search-box">--}}
{{--                    <form>--}}
{{--                        <input type="search" placeholder="Search Here.....">--}}
{{--                        <button type="submit"><i class="flaticon-search"></i>--}}
{{--                        </button>--}}
{{--                    </form> <span class="search-box-icon"><i class="flaticon-close"></i></span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!-- Hamburger Menu Start -->--}}
{{--    <div class="hamburger-popup">--}}
{{--        <div class="hamburger-close-btn">	<i class="flaticon-close"></i>--}}
{{--        </div>--}}
{{--        <div class="menu-box">--}}
{{--            <div class="menu-logo">--}}
{{--                <a href="index.html">--}}
{{--                    <img src="assets/img/logo.png" alt="">--}}
{{--                </a>--}}
{{--                <p class="pt-30">Here are many variations of passages onsectetur adipisicing elit. Voluptate, odio cupiditate? Illum, quos eos nihil harum et, id architecto dolore eius dolores culpa eveniet assumenda iusto repudiandae.</p>--}}
{{--            </div>--}}
{{--            <div class="contact-info pt-30">--}}
{{--                <h3 class="mb-25">Contact Info</h3>--}}
{{--                <div class="contact-box"> <i class="flaticon-call"></i>+(141 - 589 - 548</div>--}}
{{--                <div class="contact-box">	<i class="flaticon-envelope"></i>infoattoyer@gmail.com</div>--}}
{{--                <div class="contact-box"> <i class="flaticon-placeholder"></i> 7935 Springs, NY</div>--}}
{{--            </div>--}}
{{--            <div class="follow-us pt-30">--}}
{{--                <h3 class="mb-25">Follow Us</h3>--}}
{{--                <div class="widget social-icon d-i-block">--}}
{{--                    <ul class="footer-social">--}}
{{--                        <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>--}}
{{--                        </li>--}}
{{--                        <li><a href="#"><i class="flaticon-twitter"></i></a>--}}
{{--                        </li>--}}
{{--                        <li><a href="#"><i class="flaticon-instagram"></i></a>--}}
{{--                        </li>--}}
{{--                        <li><a href="#"><i class="flaticon-vimeo"></i></a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="hamburger-overlay"></div>--}}
{{--    <!-- Hamburger Menu Start -->--}}
{{--</div>--}}
{{--<!-- Header Menu End -->--}}
<!-- Banner Area Start -->
<div class="banner-area banner-group">
    <div class="single-slider">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="slider-content"> <span class="section-top">// We are the best</span>
                        <h2>We solve legal problems</h2>
                        <p>
                           We provide class legal service for local and international corporations.
                        </p>
                        <div class="banner-btn"> <a class="attoyer-btn" href="#">Free Consultation</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="single-slider bg-img">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="slider-content"> <span class="section-top">// We are the best</span>
                        <h2>We solve legal problems</h2>
                        <p>
                            We provide class legal service for local and international corporations.
                        </p>
                        <div class="banner-btn"> <a class="attoyer-btn" href="#">Free Consultation</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Banner Area End -->
<!-- About Area Start -->
<div class="about-area section-padding">
    <div class="bounce-top shape2">
        <img src="assets/img/shape/ab-shape-3.png" alt="">
    </div>
    <div class="item-roll shape1">
        <img src="assets/img/shape/ab-shape-2.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 md-mb-35">
                <div class="about-left">
                    <div class="about-img">
                        <img src="assets/img/about.png" alt="">
                    </div>
                    <div class="item-bounce about-shape">
                        <img src="assets/img/shape/ab-shape-1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="s-table">
                    <div class="s-tablec">
                        <div class="section-title section-title-right"> <span class="section-top">//  About Us</span>
                            <h2>We are the top law firm in Europe</h2>
                            <p class="pt-30 pb-35">

                                Wence & Partners is a leading corporate and commercial law firm in Europe with broad expertise and experience in
                                providing first-rate legal services for international and local corporations in all sectors of the economy.

                            </p>
                            <div class="about-buttom d-sm-flex">
                                <div class="about-btn"> <a class="attoyer-btn all-btn mr-100" href="#">Discover More</a>
                                </div>
                                <div class="about-signa">
                                    <img src="assets/img/signature.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Area End -->
<!-- Services Area Start -->
{{--<div class="services-area section-padding">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-xl-12 col-lg-12">--}}
{{--                <div class="section-title"> <span class="section-top">// Services</span>--}}
{{--                    <h2>Our Practice Area</h2>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="container">--}}
{{--        <div class="row align-items-center">--}}
{{--            <div class="col-xl-12 col-lg-12">--}}
{{--                <div class="services-group">--}}
{{--                    <div class="services-tiem">--}}
{{--                        <div class="services-img">--}}
{{--                            <img src="assets/img/services-1.png" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="services-content">--}}
{{--                            <h3 class="pt-20">Corporate Law</h3>--}}
{{--                            <p class="pb-10 pt-10">Here are many variations of passages consectetur elit, sed do eiusmod tempor incididunt ut labore et dolore</p> <a href="#">Read More <i class="flaticon-right-arrow"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="services-tiem">--}}
{{--                        <div class="services-img">--}}
{{--                            <img src="assets/img/services-2.png" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="services-content">--}}
{{--                            <h3 class="pt-20">Education Law</h3>--}}
{{--                            <p class="pb-10 pt-10">Here are many variations of passages consectetur elit, sed do eiusmod tempor incididunt ut labore et dolore</p> <a href="#">Read More <i class="flaticon-right-arrow"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="services-tiem">--}}
{{--                        <div class="services-img">--}}
{{--                            <img src="assets/img/services-3.png" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="services-content">--}}
{{--                            <h3 class="pt-20">Crime Law</h3>--}}
{{--                            <p class="pb-10 pt-10">Here are many variations of passages consectetur elit, sed do eiusmod tempor incididunt ut labore et dolore</p> <a href="#">Read More <i class="flaticon-right-arrow"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- Services Area End -->
<!-- Counter Area Start -->
<div class="counter-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 md-mb-30">
                <div class="counter-item">
                    <ul class="d-inline-flex align-items-center">
                        <li><i class="flaticon-court"></i>
                        </li>
                        <li><span class="counter">72</span>
                            <p>Case Done</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6">
                <div class="counter-item">
                    <ul class="d-inline-flex align-items-center">
                        <li><i class="flaticon-presentation"></i>
                        </li>
                        <li><span class="counter">59</span>
                            <p>Happy Client’s</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 sm-mt-30 sm-mb-30">
                <div class="counter-item">
                    <ul class="d-inline-flex align-items-center">
                        <li><i class="flaticon-law"></i>
                        </li>
                        <li><span class="counter">42</span>
                            <p>Case Dismissed</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6">
                <div class="counter-item">
                    <ul class="d-inline-flex align-items-center">
                        <li><i class="flaticon-balance"></i>
                        </li>
                        <li><span class="counter">92</span>
                            <p>Charges Dropped</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Counter Area End -->
<!-- Why Choose Ua Area Area Start -->
<div class="why-choose-area section-padding">
    <div class="item-roll choose1">
        <img src="assets/img/shape/ab-shape-02.png" alt="">
    </div>
    <div class="item-bounce choose2">
        <img src="assets/img/shape/ch-shape-2.png" alt="">
    </div>
    <div class="why-choose-img"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-8">
                <div class="section-title-left"> <span class="section-top">// Why Choose Us</span>
                    <h2 class="mb-55">Why We different from others</h2>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 mb-40">
                            <div class="choose-item d-inline-flex">
                                <div class="choose-icon"> <i class="flaticon-checked"></i>
                                </div>
                                <div class="choose-content">
                                    <h3>Intellectual Property</h3>
                                    <p>We practice intellectual property litigation, including patent, trademark etc.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="choose-item d-inline-flex">
                                <div class="choose-icon"> <i class="flaticon-checked"></i>
                                </div>
                                <div class="choose-content">
                                    <h3>Litigation</h3>
                                    <p> We handle a wide variety of cases, including complex commercial transactions,
                                        consumer litigation, class actions etc.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 sm-mt-30 sm-mb-30">
                            <div class="choose-item d-inline-flex">
                                <div class="choose-icon"> <i class="flaticon-checked"></i>
                                </div>
                                <div class="choose-content">
                                    <h3>Business Law</h3>
                                    <p>
                                        We provide services as outside general counsel, advising leading local and national corporations
                                        and other businesses on a full range of corporate law matters.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="choose-item d-inline-flex">
                                <div class="choose-icon"> <i class="flaticon-checked"></i>
                                </div>
                                <div class="choose-content">
                                    <h3>Employment and Labour Law </h3>
                                    <p>We have a team of dedicated Employment & Labor Law practitioners to handle our clients’
                                        most complex employment cases.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Why Choose Ua Area Area End -->
<!-- Team Area Start -->
<div class="team-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="section-title"> <span class="section-top">// Expert Team</span>
                    <h2>Meet Expert Team</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-lg-3 col-md-6 md-mb-30">
                <div class="team-item">
                    <div class="team-img">
                        <img src="assets/img/team-1.png" alt="">
                        <div class="team-social">
                            <ul>
                                <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>
                                </li>
                                <li><a href="#"><i class="flaticon-twitter"></i></a>
                                </li>
                                <li><a href="#"><i class="flaticon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content">
                        <h3>Michelle Emily</h3>
                        <p>Lawyer</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-lg-3 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="assets/img/team-2.png" alt="">
                        <div class="team-social">
                            <ul>
                                <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>
                                </li>
                                <li><a href="#"><i class="flaticon-twitter"></i></a>
                                </li>
                                <li><a href="#"><i class="flaticon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content">
                        <h3>Al Mahmud</h3>
                        <p>Lawyer</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-lg-3 col-md-6 sm-mt-30 sm-mb-30">
                <div class="team-item">
                    <div class="team-img">
                        <img src="assets/img/team-3.png" alt="">
                        <div class="team-social">
                            <ul>
                                <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>
                                </li>
                                <li><a href="#"><i class="flaticon-twitter"></i></a>
                                </li>
                                <li><a href="#"><i class="flaticon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content">
                        <h3>William Michael</h3>
                        <p>Lawyer</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-lg-3 col-md-6">
                <div class="team-item">
                    <div class="team-img">
                        <img src="assets/img/team-4.png" alt="">
                        <div class="team-social">
                            <ul>
                                <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>
                                </li>
                                <li><a href="#"><i class="flaticon-twitter"></i></a>
                                </li>
                                <li><a href="#"><i class="flaticon-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="team-content">
                        <h3>Lauren Linda</h3>
                        <p>Lawyer</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Team Area End -->
<!-- Solution Area Start -->
<div class="solution-area section-padding">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6 col-md-12 md-mb-30">
                <div class="section-title-left">
                    <h2>We are always ready for your solution</h2>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12">
                <div class="solution-right">
                    <div class="solution-icon">
                        <div class="icon"><i class="flaticon-call"></i>
                        </div>
                        <div class="solution-number">
                            <p>+1 41 - 589 - 548</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Solution Area End -->
<!-- Testimonial Area Start -->
{{--<div class="testimonial-area section-padding">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-xl-4 col-lg-5 col-md-6 sm-mb-30">--}}
{{--                <div class="section-title-left"> <span class="section-top">// Testimonial</span>--}}
{{--                    <h2>What they are Talking About Lawyer</h2>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-xl-8 col-lg-7 col-md-6">--}}
{{--                <div class="testimonial-group">--}}
{{--                    <div class="testimonial-tiem">--}}
{{--                        <div class="testimonial-icon"> <i class="flaticon-straight-quotes"></i>--}}
{{--                        </div>--}}
{{--                        <p class="pb-30">Here are many variations of passages consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>--}}
{{--                        <div class="client-name d-inline-flex">--}}
{{--                            <div class="client-img mr-20">--}}
{{--                                <img src="assets/img/testminial-1.png" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="client-content">--}}
{{--                                <h3>Al Mahmud</h3>--}}
{{--                                <p>Client</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="testimonial-tiem">--}}
{{--                        <div class="testimonial-icon"> <i class="flaticon-straight-quotes"></i>--}}
{{--                        </div>--}}
{{--                        <p class="pb-30">Here are many variations of passages consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>--}}
{{--                        <div class="client-name d-inline-flex">--}}
{{--                            <div class="client-img mr-20">--}}
{{--                                <img src="assets/img/testminial-2.png" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="client-content">--}}
{{--                                <h3>Jhon Deo</h3>--}}
{{--                                <p>Client</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="testimonial-tiem">--}}
{{--                        <div class="testimonial-icon"> <i class="flaticon-straight-quotes"></i>--}}
{{--                        </div>--}}
{{--                        <p class="pb-30">Here are many variations of passages consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</p>--}}
{{--                        <div class="client-name d-inline-flex">--}}
{{--                            <div class="client-img mr-20">--}}
{{--                                <img src="assets/img/testminial-3.png" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="client-content">--}}
{{--                                <h3>Al Mahmud</h3>--}}
{{--                                <p>Client</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- Testimonial Area End -->
<!-- Faq Area Start -->
<div class="faq-area section-padding">
    <div class="item-bounce faq1">
        <img src="assets/img/shape/fq-shape-1.png" alt="">
    </div>
    <div class="bounce-top faq2">
        <img src="assets/img/shape/fq-shape-2.png" alt="">
    </div>
    <div class="faq-img"></div>

    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6"></div>
            <div class="col-xl-6 col-lg-6">
                <div class="section-title section-title-right"> <span class="section-top">//  FAQ</span>
                    <h2 class="mb-60">Frequently asked questions</h2>
                </div>
                <div class="collapse-area">
                    <div class="collapse-item">
                        <div class="collapse-card">
                            <div class="collapse-header">
                                <h3>1. Why should I trust your firm with my case??</h3>
                                <i class="flaticon-minus"></i>
                            </div>
                            <div class="card-body active">
                                <p>
                                    Choosing a law firm to represent you and your case is one of the most important decisions you can make.
                                    We understand that your case is important to you and choosing the right firm and attorneys to represent you could
                                    make the difference for your case.

                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="collapse-item mb-25 mt-25">
                        <div class="collapse-card">
                            <div class="collapse-header">
                                <h3>2. What types of cases does your law firm handle?</h3>
                                <i class="flaticon-plus"></i>
                            </div>
                            <div class="card-body" style="display: none;">
                                <p>we handle claims at all levels of litigation. Our areas of practice include Business Law,
                                    Employment and Labor Law, Intellectual Property/Technology Transactions, and Litigation.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="collapse-item">
                        <div class="collapse-card">
                            <div class="collapse-header">
                                <h3>3.  What are your business hours?</h3>
                                <i class="flaticon-plus"></i>
                            </div>
                            <div class="card-body" style="display: none;">
                                <p>
                                    ur business hours are 8:30 am to 6:00 pm Monday through Friday. However, please feel free to call or email us after hours.
                                    We are committed to returning our client’s and potential client’s calls and emails within 24 hours or less.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Faq Area End -->
<!-- Contact Area Start -->
<div class="contact-area section-padding">
    <div class="contact-right"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="section-title-left"> <span class="section-top">//  Free Quote</span>
                    <h2 class="mb-60">Get a free Consultation</h2>
                    <div class="from-area">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" id="fullName" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" class="form-control" id="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="subject" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="message" rows="2" placeholder="Message"></textarea>
                            </div>
                            <button type="submit" class="attoyer-btn">Send Massage</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact Area End -->
<!-- Blog Area Start -->
<div class="blog-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="section-title">	<span class="section-top">//  New & Blog</span>
                    <h2>Everyday update here</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 md-mb-30">
                <div class="blog-item">
                    <div class="blog-img">
                        <img src="assets/img/blog-1.png" alt="">
                    </div>
                    <div class="blog-content">
                        <ul>
                            <li><i class="flaticon-date"></i> 29, June 2021</li>
                            <li><i class="flaticon-comment"></i> Comments 3</li>
                        </ul>
                        <h3><a href="blog-details.html">Internships, things to do Advice on a career in</a></h3>
                        <a href="#">Load More <i class="flaticon-right-arrow"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 sm-mb-30">
                <div class="blog-item">
                    <div class="blog-img">
                        <img src="assets/img/blog-2.png" alt="">
                    </div>
                    <div class="blog-content">
                        <ul>
                            <li><i class="flaticon-date"></i> 15, June 2021</li>
                            <li><i class="flaticon-comment"></i> Comments 3</li>
                        </ul>
                        <h3><a href="blog-details.html">Latest legal news, comment and analysis</a></h3>
                        <a href="#">Load More <i class="flaticon-right-arrow"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="blog-item">
                    <div class="blog-img">
                        <img src="assets/img/blog-3.png" alt="">
                    </div>
                    <div class="blog-content">
                        <ul>
                            <li><i class="flaticon-date"></i> 29, June 2021</li>
                            <li><i class="flaticon-comment"></i> Comments 3</li>
                        </ul>
                        <h3><a href="blog-details.html">Day Care Responsibility to Protect Children</a></h3>
                        <a href="#">Load More <i class="flaticon-right-arrow"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Blog Area End -->

@endsection
