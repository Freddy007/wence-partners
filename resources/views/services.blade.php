@extends("layouts.main-layout")

@section("content")

<body>

<!-- Page Banner Start -->
<div class="page-banner section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="page-title">
                    <h1 class="pb-30">Services-1</h1>
                    <ul>
                        <li><a href="#">Hone</a>
                        </li>
                        <li><a href="#">Services-1</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Banner End -->
<!-- Services Area Start -->
{{--<div class="services-area section-padding">--}}
{{--    <div class="container">--}}
{{--        <div class="row align-items-center">--}}
{{--            <div class="col-xl-12 col-lg-12">--}}
{{--                <div class="services-group">--}}
{{--                    <div class="services-tiem">--}}
{{--                        <div class="services-img">--}}
{{--                            <img src="assets/img/services-1.png" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="services-content">--}}
{{--                            <h3 class="pt-20">Corporate Law</h3>--}}
{{--                            <p class="pb-10 pt-10">Here are many variations of passages consectetur elit, sed do eiusmod tempor incididunt ut labore et dolore</p> <a href="#">Read More <i class="flaticon-right-arrow"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="services-tiem">--}}
{{--                        <div class="services-img">--}}
{{--                            <img src="assets/img/services-2.png" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="services-content">--}}
{{--                            <h3 class="pt-20">Education Law</h3>--}}
{{--                            <p class="pb-10 pt-10">Here are many variations of passages consectetur elit, sed do eiusmod tempor incididunt ut labore et dolore</p> <a href="#">Read More <i class="flaticon-right-arrow"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="services-tiem">--}}
{{--                        <div class="services-img">--}}
{{--                            <img src="assets/img/services-3.png" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="services-content">--}}
{{--                            <h3 class="pt-20">Crime Law</h3>--}}
{{--                            <p class="pb-10 pt-10">Here are many variations of passages consectetur elit, sed do eiusmod tempor incididunt ut labore et dolore</p> <a href="#">Read More <i class="flaticon-right-arrow"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}


<!-- Services Area End -->
<!-- Solution Area Start -->
{{--<div class="solution-area section-padding">--}}
{{--    <div class="container">--}}
{{--        <div class="row align-items-center">--}}
{{--            <div class="col-xl-6 col-lg-6 col-md-12 md-mb-30">--}}
{{--                <div class="section-title-left">--}}
{{--                    <h2>We are always ready for your solution</h2>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-xl-6 col-lg-6 col-md-12">--}}
{{--                <div class="solution-right">--}}
{{--                    <div class="solution-icon">--}}
{{--                        <div class="icon"><i class="flaticon-call"></i>--}}
{{--                        </div>--}}
{{--                        <div class="solution-number">--}}
{{--                            <p>+ (141 - 589 - 548</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- Solution Area End -->
<!-- About Area Start -->
<div class="about-area section-padding">
    <div class="bounce-top shape2">
        <img src="assets/img/shape/ab-shape-3.png" alt="">
    </div>
    <div class="item-roll shape1">
        <img src="assets/img/shape/ab-shape-2.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 md-mb-35">
                <div class="about-left">
                    <div class="about-img">
                        <img src="assets/img/about.png" alt="">
                    </div>
                    <div class="item-bounce about-shape">
                        <img src="assets/img/shape/ab-shape-1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="s-table">
                    <div class="s-tablec">
                        <div class="section-title section-title-right"> <span class="section-top">//  About Us</span>
                            <h2>We're top law Company in last 10 years</h2>
                            <p class="pt-30 pb-35">Here are many variations of passages consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud Here are many variations of passages consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
                            <div class="about-buttom d-sm-flex">
                                <div class="about-btn"> <a class="attoyer-btn all-btn mr-100" href="#">Discover More</a>
                                </div>
                                <div class="about-signa">
                                    <img src="assets/img/signature.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Area End -->

<!-- scroll_up_btn -->
<div class="scroll-top"> <i class="flaticon-up-arrow"></i>
</div>
<!-- scroll_up_btn end -->
<!-- Main JS -->
<script src="assets/js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Popper JS -->
<script src="assets/js/popper.min.js"></script>
<!-- Carousel -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- WOW JS -->
<script src="assets/js/wow.min.js"></script>
<!-- Counterup -->
<script src="assets/js/jquery.counterup.min.js"></script>
<!-- Waypoints -->
<script src="assets/js/waypoints.min.js"></script>
<!-- jquery.slicknav.min -->
<script src="assets/js/jquery.slicknav.min.js"></script>
<!-- Mean menu -->
<script src="assets/js/jquery.meanmenu.min.js"></script>
<!-- Custom JS -->
<script src="assets/js/custom.js"></script>
</body>


</html>

@endsection
