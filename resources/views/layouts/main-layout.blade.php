<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <!-- Start Meta -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="ThemeOri">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Title of Site -->
    <title>Wence Partners </title>
    <!-- Favicons -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="assets/css/animate.css">
    <!-- slicknav -->
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <!-- Mean menu -->
    <link rel="stylesheet" href="assets/css/meanmenu.min.css">
    <!-- Flaticon -->
    <link rel="stylesheet" href="assets/fonts/flaticon.css">
    <!-- Space -->
    <link rel="stylesheet" href="assets/css/space.css">
    <!-- Default-->
    <link rel="stylesheet" href="assets/css/default.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="assets/css/responsive.css">
</head>

<body>
<!-- Preloader start -->
{{--<div class="theme-loader">--}}
{{--    <div class="spinner">--}}
{{--        <div class="double-bounce1"></div>--}}
{{--        <div class="double-bounce2"></div>--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Top Bar Start -->
<div class="top-bar">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6 col-md-8">
                <div class="top-left d-i-block">
                    <ul>
                        <li><a href="#"><i class="flaticon-call"></i></a> +(141 - 589 - 548</li>
                        <li><a href="#"><i class="flaticon-envelope"></i></a> info@wencepartners.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-4">
                <div class="top-right d-i-block text-right">
                    <ul>
                        <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>
                        </li>
                        <li><a href="#"><i class="flaticon-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="flaticon-linkedin"></i></a>
                        </li>
                        <li><a href="#"><i class="flaticon-vimeo"></i></a>
                        </li>
                        <li><a href="#"><i class="flaticon-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Top Bar End -->
<!-- Header Menu Start -->
<div class="header-menu">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-2 col-lg-2 col-md-7">
                <div class="menu-logo">
                    <a href="{{route("home")}}">
                        <img src="assets/img/logo.png" alt="">
                    </a>
                </div>
                <div class="responsive-menu"></div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-2">
                <div class="main-menu">
                    <ul id="mobilemenu">
                        <li class="menu-icon"><a href="{{route("home")}}">Home</a></li>

                        <li class="menu-icon"><a href="{{route("about")}}">About Us</a></li>

{{--                        <li class="menu-icon"><a href="{{route("services")}}">Services</a></li>--}}

                        <li class="menu-icon"><a href="{{route("contact-us")}}">Contact</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-xl-1 col-lg-1 col-md-3">
                <div class="menu-icon">
                    <ul class="d-flex">
                        <li><span class="search-icon open"><i class="flaticon-search"></i></span>
                        </li>
                        <li>
                            <div class="hamburger-icon"> <i class="flaticon-menu"></i>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="search-box">
                    <form>
                        <input type="search" placeholder="Search Here.....">
                        <button type="submit"><i class="flaticon-search"></i>
                        </button>
                    </form> <span class="search-box-icon"><i class="flaticon-close"></i></span>
                </div>
            </div>
        </div>
    </div>
    <!-- Hamburger Menu Start -->
    <div class="hamburger-popup">
        <div class="hamburger-close-btn">	<i class="flaticon-close"></i>
        </div>
        <div class="menu-box">
            <div class="menu-logo">
                <a href="index.html">
                    <img src="assets/img/logo.png" alt="">
                </a>
                <p class="pt-30">.</p>
            </div>
            <div class="contact-info pt-30">
                <h3 class="mb-25">Contact Info</h3>
                <div class="contact-box"> <i class="flaticon-call"></i>+(141 - 589 - 548</div>
                <div class="contact-box">	<i class="flaticon-envelope"></i>info@wencepartners.com</div>
                <div class="contact-box"> <i class="flaticon-placeholder"></i> 7935 Springs, NY</div>
            </div>
            <div class="follow-us pt-30">
                <h3 class="mb-25">Follow Us</h3>
                <div class="widget social-icon d-i-block">
                    <ul class="footer-social">
                        <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>
                        </li>
                        <li><a href="#"><i class="flaticon-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="flaticon-instagram"></i></a>
                        </li>
                        <li><a href="#"><i class="flaticon-vimeo"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="hamburger-overlay"></div>
    <!-- Hamburger Menu Start -->
</div>
<!-- Header Menu End -->

@yield('content')

<!-- Subscribe Area Start  -->
<div class="subscribe-now">
    <div class="container">
        <div class="row subscribe-bg align-items-center">
            <div class="subscribe-shape"></div>
            <div class="col-xl-5 col-lg-5">
                <div class="subscribe-left md-mb-25">
                    <h3>Subscribe now to Stay with Us</h3>
                </div>
            </div>
            <div class="col-xl-7 col-lg-7">
                <div class="subscribe-right">
                    <form class="subscribe-box" action="#">
                        <input class="form-control" placeholder="Email" type="text">
                        <button type="submit" class="subscribe-button">Subscribe Now</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Subscribe Area End -->

<!-- Footer Area Start  -->
<div class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 md-mb-30">
                <div class="footer-widget">
                    <h3>About Us</h3>
                    <div class="widget about-info">
                        <p>
                            Wence & Partners is a leading corporate and commercial law firm in Europe with broad expertise and experience in
                            providing first-rate legal services for international and local corporations in all sectors of the economy.

                        </p>
                    </div>
                    <div class="widget social-icon">
                        <ul class="footer-social">
                            <li><a href="#"><i class="flaticon-facebook-app-symbol"></i></a>
                            </li>
                            <li><a href="#"><i class="flaticon-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="flaticon-instagram"></i></a>
                            </li>
                            <li><a href="#"><i class="flaticon-vimeo"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="footer-widget">
                    <h3>Recent Post</h3>
                    <div class="widget blog-widget">
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="assets/img/fb-1.png" alt="">
                                </a>
                                <h4><a href="#">Essential for the Construction</a></h4>
                                <i class="flaticon-date"></i><span>12 Jan 2019</span>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="assets/img/fb-2.png" alt="">
                                </a>
                                <h4><a href="#">Essential for the Construction</a></h4>
                                <i class="flaticon-date"></i><span>28 Jan 2019</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 sm-mt-30 sm-mb-30">
                <div class="footer-widget">
                    <h3>Quick Links</h3>
                    <div class="widget footer-menu">
                        <ul>
                            <li><a href="{{route("home")}}"> Home</a></li>

                            <li><a href="{{route("about")}}"><i class="flaticon-double-right-arrows-angles"></i> About Us</a>
                            </li>
{{--                            <li><a href="{{route("services")}}"><i class="flaticon-double-right-arrows-angles"></i> Services</a>--}}
{{--                            </li>--}}
                            <li><a href="{{route("contact-us")}}"><i class="flaticon-double-right-arrows-angles"></i> Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6">
                <div class="footer-widget">
                    <h3>Basic Info</h3>
                    <div class="widget basic-info">
                        <ul>
                            <li><i class="flaticon-placeholder"></i> 7935 Springs, NY</li>
                            <li><i class="flaticon-call"></i> +(141 - 589 - 548</li>
                            <li><i class="flaticon-envelope"></i> info@wencepartners.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Area End -->

<!-- Copyright Area Start -->
<div class="footer-copyright">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="copyright-content">
                    <p>© 2021. Wence Partners. All rights reserved</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Copyright Area End -->
<!-- scroll_up_btn -->
<div class="scroll-top"> <i class="flaticon-up-arrow"></i>
</div>
<!-- scroll_up_btn end -->

<!-- Main JS -->
<script src="assets/js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Popper JS -->
<script src="assets/js/popper.min.js"></script>
<!-- Carousel -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- WOW JS -->
<script src="assets/js/wow.min.js"></script>
<!-- Counterup -->
<script src="assets/js/jquery.counterup.min.js"></script>
<!-- Waypoints -->
<script src="assets/js/waypoints.min.js"></script>
<!-- jquery.slicknav.min -->
<script src="assets/js/jquery.slicknav.min.js"></script>
<!-- Mean menu -->
<script src="assets/js/jquery.meanmenu.min.js"></script>
<!-- Custom JS -->
<script src="assets/js/custom.js"></script>


</body>

</html>
