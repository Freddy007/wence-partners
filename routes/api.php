<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'API'], function () {

    Route::group(['middleware' => 'auth:api'], function() {
        Route::post("/user-details",[App\Http\Controllers\API\UserController::class, 'postDetails'])->name("user-details");
    });

    Route::post("/refresh",[App\Http\Controllers\API\UserController::class, 'postUpdate'])->name("refresh-token");
});
